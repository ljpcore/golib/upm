package upm

import (
	"errors"
	"sync"
)

// A Lookup is the core type exported by this module.  Values can be registered
// against the Lookup under a specified pattern.  These values can then be
// retrieved by a path that matches that pattern.
//
// Lookup occurs in O(n) where n is the number of segments in the path.
//
// This type is fully thread-safe.
type Lookup struct {
	rootLayer *layer
	mx        *sync.RWMutex
}

// NewLookup creates a new, empty Lookup.
func NewLookup() *Lookup {
	return &Lookup{
		rootLayer: newLayer(),
		mx:        &sync.RWMutex{},
	}
}

// Register registers a value in the Lookup under the specified pattern.  If the
// value to register is nil, or the pattern contains no tokens, or the pattern
// contains a multi-segment wildcard (**) that is not the last segment in the
// pattern, an error is returned.
func (l *Lookup) Register(pattern string, value interface{}) error {
	if value == nil {
		return errors.New("the provided value is nil")
	}

	tokens := tokenizePath(pattern)
	for i, token := range tokens {
		if token == "**" && i != len(tokens)-1 {
			return errors.New("the provided pattern contains a multi-segment wildcard that is not the last segment in the pattern")
		}
	}

	l.mx.Lock()
	defer l.mx.Unlock()

	l.rootLayer.Register(tokens, value)

	return nil
}

// Retrieve attempts to pull a value from the lookup using the provided path.
// The wildcard segments used to match the path, if any, are also returned.  The
// returned interface will be nil if no match was found.
func (l *Lookup) Retrieve(path string) (interface{}, []string) {
	l.mx.RLock()
	defer l.mx.RUnlock()

	tokens := tokenizePath(path)
	return l.rootLayer.Retrieve(tokens, []string{})
}

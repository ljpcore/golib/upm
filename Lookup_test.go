package upm

import (
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/has"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestLookupE2E(t *testing.T) {
	testCases := []struct {
		path     string
		val      interface{}
		segments []string
	}{
		{path: " 1/2/3 ", val: "a", segments: []string{}},
		{path: "  1/1/4", val: "b", segments: []string{"1"}},
		{path: " 1/2/4", val: "b", segments: []string{"2"}},
		{path: "1/2/5", val: "c", segments: []string{"2", "5"}},
		{path: "2/6", val: nil, segments: []string{}},

		{path: "/customer/a/b/delete", val: "d", segments: []string{"a", "b"}},
	}

	x := NewLookup()

	test.That(t, x.Register("/1/2/3", "a"), is.Nil)
	test.That(t, x.Register("/1/*/4", "b"), is.Nil)
	test.That(t, x.Register("/1/**", "c"), is.Nil)

	test.That(t, x.Register("/customer/*/*/delete", "d"), is.Nil)
	test.That(t, x.Register("/customer/*/*/*", "e"), is.Nil)

	for _, testCase := range testCases {
		val, segments := x.Retrieve(testCase.path)
		test.That(t, val, is.EqualTo(testCase.val))

		test.That(t, segments, has.Length(len(testCase.segments)))
		for _, expectedSegment := range testCase.segments {
			test.That(t, segments, has.ValueThat(is.EqualTo(expectedSegment)))
		}
	}
}

func TestLookupRegisterFailures(t *testing.T) {
	testCases := []struct {
		pattern  string
		value    interface{}
		expected string
	}{
		{pattern: "/**/a", value: "a", expected: "the provided pattern contains a multi-segment wildcard that is not the last segment in the pattern"},
		{pattern: "/a/b", value: nil, expected: "the provided value is nil"},
	}

	x := NewLookup()

	for _, testCase := range testCases {
		err := x.Register(testCase.pattern, testCase.value)
		test.That(t, err, is.NotNil)
		test.That(t, err.Error(), is.EqualTo(testCase.expected))
	}
}

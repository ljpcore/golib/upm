package upm

// layer is the data type that drives the functionality of Lookup.  Each layer
// optionally has an associated value, as well as a map of sub-layers, which may
// be empty.
//
// This type is not thread-safe.  Lookup implements the thread-safety directly.
type layer struct {
	sublayers map[string]*layer
	value     interface{}
}

// newLayer creates a new, empty layer.
func newLayer() *layer {
	return &layer{
		sublayers: map[string]*layer{},
	}
}

// Register registers the provided value using the given tokens.  We rely on
// Lookup to do validation here, so we must accept that the inputs are valid.
func (l *layer) Register(tokens []string, value interface{}) {
	if len(tokens) < 1 {
		l.value = value
		return
	}

	token := tokens[0]
	sublayer, ok := l.sublayers[token]
	if !ok {
		sublayer = newLayer()
	}

	sublayer.Register(tokens[1:], value)
	l.sublayers[token] = sublayer
}

// Retrieve does the actual lookup in the data structure.  It will prioritize
// exact matches, then wildcards, then multi-segment wildcards.
func (l *layer) Retrieve(tokens []string, originalSegments []string) (interface{}, []string) {
	if len(tokens) < 1 {
		return l.value, originalSegments
	}

	// 1. Exact Match.
	token := tokens[0]
	sublayer, ok := l.sublayers[token]
	if ok {
		val, segments := sublayer.Retrieve(tokens[1:], originalSegments)
		if val != nil {
			return val, segments
		}
	}

	// 2. Wildcard Match.
	sublayer, ok = l.sublayers["*"]
	if ok {
		newSegments := append(originalSegments, token)
		val, segments := sublayer.Retrieve(tokens[1:], newSegments)
		if val != nil {
			return val, segments
		}
	}

	// 3. Multi-segment Wildcard Match.
	sublayer, ok = l.sublayers["**"]
	if ok {
		newSegments := append(originalSegments, tokens...)
		val, segments := sublayer.Retrieve(nil, newSegments)
		if val != nil {
			return val, segments
		}
	}

	return nil, originalSegments
}

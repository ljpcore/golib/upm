package upm

import "strings"

func tokenizePath(path string) []string {
	path = strings.TrimSpace(path)
	path = strings.ReplaceAll(path, "\\", "/")
	tokens := strings.Split(path, "/")
	nonEmptyTokens := make([]string, 0, len(tokens))

	for _, token := range tokens {
		if len(token) == 0 {
			continue
		}
		nonEmptyTokens = append(nonEmptyTokens, token)
	}

	return nonEmptyTokens
}

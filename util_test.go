package upm

import (
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/has"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestTokenizePath(t *testing.T) {
	testCases := []struct {
		g string
		e []string
	}{
		{g: "	/test/1234/*\\5 ", e: []string{"test", "1234", "*", "5"}},
		{g: "", e: []string{}},
		{g: "/", e: []string{}},
		{g: "test", e: []string{"test"}},
		{g: "/test", e: []string{"test"}},
		{g: "/test/", e: []string{"test"}},
		{g: "\\hello\\world ", e: []string{"hello", "world"}},
	}

	for _, testCase := range testCases {
		a := tokenizePath(testCase.g)
		test.That(t, a, has.Length(len(testCase.e)))

		for _, e := range testCase.e {
			test.That(t, a, has.ValueThat(is.EqualTo(e)))
		}
	}
}
